#!/usr/bin/env bash

# Compile the assets
rake assets:precompile

# Start the server
rails server -b 0.0.0.0
