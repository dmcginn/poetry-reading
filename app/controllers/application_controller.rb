require 'net/http'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def get_poem_text(text_api_code)
    url = "#{Rails.configuration.api_base_url}/v1/poems/#{text_api_code}.json?api-key=#{Rails.configuration.api_key}"

    begin
      url = URI.parse(url)
      req = Net::HTTP::Get.new(url.to_s)
      res = Net::HTTP.start(url.host, url.port) {|http|
        http.request(req)
      }
      text = JSON.parse(res.body)
      text = text["poem"]["text"]
      return text
    rescue Exception => e
      return nil
    end
  end
end