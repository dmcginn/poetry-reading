module Api
  class PoemsController < ApplicationController
    def index
      sql = 'select poems.title as poem, readings.url as url, poets.name as poet, readers.name as reader from readings inner join readers on readings.reader_id = readers.id inner join poems on readings.poem_id = poems.id inner join poets on poems.poet_id = poets.id where readings.active IS TRUE order by readings.id desc'
      result = ActiveRecord::Base.connection.execute(sql)

      render json: result, status: :ok
    end
  end
end
