class CollectionsController < ApplicationController
  before_action :set_collection, only: [:show]

  # def index
  # end

  def show
  end

  private
  def set_collection
    @collection = Collection.friendly.find(params[:id])
  end
end
