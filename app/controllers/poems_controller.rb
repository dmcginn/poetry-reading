class PoemsController < ApplicationController
  before_action :set_poem, only: [:show, :edit, :update, :destroy]

  # GET /poems
  # GET /poems.json
  def index
    @poems = Poem.order(:title).group_by { |poem| poem.title[0] }
  end

  # GET /poems/1
  # GET /poems/1.json
  def show
    @readings = @poem.readings
    #@poem_text = get_poem_text(@poem.text_api_code) if @poem.text_api_code
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_poem
      @poem = Poem.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def poem_params
      params.require(:poem).permit(:title, :poet_id)
    end
end
