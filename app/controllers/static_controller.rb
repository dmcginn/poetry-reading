class StaticController < ApplicationController
  def index
    @latest_readings = Reading.last(6).reverse
    @featured_poet = Poet.where(:name => "Maya Angelou").first
    @featured_reader = Reader.where(:name => "Christopher Hitchens").first

    render :layout => 'homepage'
  end

  def search
    @poets = search_poets
    @poems = search_poems
    @readers = search_readers
  end

  def about
  end

  def update_data
    done = 'no'
    if params[:key] == 'idjvnvu2n32328728uhybufgv!!112__JJDkd'
      require 'rake'
      PoetryReading::Application.load_tasks
      Rake::Task['data:add_from_google'].invoke
      done = 'yes'
    end

    render json: { 'done' => done }, status: :not_found
  end

  protected

  def search_poets
    if params[:q].present?
      Poet.where("name ILIKE ?", "%#{params[:q]}%")
    end
  end

  def search_poems
    if params[:q].present?
      Poem.where("title ILIKE ?", "%#{params[:q]}%")
    end
  end

  def search_readers
    if params[:q].present?
      Reader.where("name ILIKE ?", "%#{params[:q]}%")
    end
  end
end




# def index
#   @candidates = candidates
#   @pagetitle = 'Candidates'
#   respond_to do |format|
#     format.html
#     format.json { render_for_api :default, json: @candidates }
#     format.xml { render_for_api :default, xml: @candidates }
#     format.csv do
#       headers['Content-Disposition'] = "attachment; filename=\"candidates-list\""
#       headers['Content-Type'] ||= 'text/csv'
#     end
#   end
# end

# def show
#   @candidate = Candidate.friendly.find(params[:id])
#   @pagetitle = @candidate.full_name
#   respond_to do |format|
#     format.html
#     format.json { render_for_api :default, json: @candidate }
#     format.xml { render_for_api :default, xml: @candidate }
#   end
# end

# protected

# def candidates
#   if params[:q].present?
#     Candidate.search(params[:q])
#   elsif params[:constituency_id].present?
#     Constituency.friendly.find(params[:constituency_id]).candidates
#   elsif params[:party_id].present?
#     Party.friendly.find(params[:party_id]).candidates
#   else
#     Candidate.list.order(last_name: :asc)
#   end
# end
