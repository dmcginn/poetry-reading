module ApplicationHelper

  def brand_icon(url)
    # Father forgive me

    # Get which brand is in URL
    if url.include?("youtu")
      return '<i title="YouTube link" class="fab fa-2x fa-youtube-square"></i>'.html_safe
    elsif url.include?("soundcloud")
      return '<i title="Soundcloud link" class="fab fa-2x fa-soundcloud"></i>'.html_safe;
    elsif url.include?("instagram")
      return '<i title="Instagram link" class="fab fa-2x fa-instagram"></i>'.html_safe;
    elsif url.include?("vimeo")
      return '<i title="Vimeo link" class="fab fa-2x fa-vimeo"></i>'.html_safe;
    else
      return nil
    end

    # Return appropriate HTML icon


    # icon = nil

    # icon = case url
    # when url.scan(/youtu/).size > 0
    #   return '<i title="YouTube link" class="fa fa-2x fa-youtube-square"></i>'.html_safe
    # when url.scan(/soundcloud/).size > 0
    #   return '<i title="Soundcloud link" class="fa fa-2x fa-soundcloud"></i>'.html_safe;
    # end

  end
end
