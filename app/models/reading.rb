require 'soundcloud'

class Reading < ActiveRecord::Base
  belongs_to :reader
  belongs_to :poem
  has_many :collection_readings
  has_many :collections, through: :collection_readings
  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }

  def embeddable
    # Determine if youtube or soundcloud
    url = read_attribute(:url)
    return nil if url.nil?

    url_stripped = url.gsub(/[^0-9a-z ]/i, '')

    if url_stripped.include?("soundcloud")
      id = get_embeddable_id_from_url(url, "soundcloud")
      return embeddable_html(id, "soundcloud")
    elsif url_stripped.include?("youtube")
      id = get_embeddable_id_from_url(url, "youtube")
      return embeddable_html(id, "youtube")
    elsif url_stripped.include?("instagram")
      id = get_embeddable_id_from_url(url, "instagram")
      return embeddable_html(id, "instagram")
    elsif url_stripped.include?("vimeo")
      id = get_embeddable_id_from_url(url, "vimeo")
      return embeddable_html(id, "vimeo")
    else
      return nil
    end
  end

  def platform
    url = read_attribute(:url)
    return nil if url.nil?

    url_stripped = url.gsub(/[^0-9a-z ]/i, '')

    return 'soundcloud' if url_stripped.include?("soundcloud")
    return 'youtube' if url_stripped.include?("youtube")
    return 'instagram' if url_stripped.include?("instagram")
    return 'vimeo' if url_stripped.include?("vimeo")
    return 'twitter' if url_stripped.include?("twitter")

    raise PlatformUnknown, "Platform unknown for URL #{url}"
  end

  def platform_id
    get_embeddable_id_from_url(url, platform)
  end

  def dead?
    send("#{platform}_dead?", platform_id)
  end

  private

  def youtube_dead?(id)
    video = Yt::Video.new id: id
    video.title
    false
  rescue Yt::Errors::NoItems
    true
  end

  def soundcloud_dead?(id)
    false
  end

  def instagram_dead?(id)
    false
  end

  def vimeo_dead?(id)
    false
  end

  def twitter_dead?(id)
    false
  end

  def link_type
  end

  def get_embeddable_id_from_url(url, host)
    if host=='youtube'
      id_array = /\b(youtube.com\/watch\?v=|youtu.be\/)(.*)/.match(url)
      return id_array[2]
    elsif host=='soundcloud'
      soundcloud = Soundcloud.new(:client_id => '8d1843b4bc9a2cb7b943c4a536dada83', :client_secret => 'e0685e5e407643f40f2f0c485bea16a0')
      track = soundcloud.get('/resolve', :url => url)
      return track.id
    elsif host=='instagram'
      id_array = /instagram.com\/\w{1,2}\/([a-zA-Z\-0-9]+)/.match(url)
      return id_array[1]
    elsif host=='vimeo'
      id_array = /vimeo.com\/([0-9]+)/.match(url)
      return id_array[1]
    else
      return nil
    end
  end

  def embeddable_html(id, host)
    if host=='youtube'
      html = "<div class='youtube-container'><iframe class='youtube-video' src='https://www.youtube.com/embed/#{id}' frameborder='0' allowfullscreen></iframe></div>"
      return html.html_safe
    elsif host=='soundcloud'
      html = "<iframe width='100%' height='300' scrolling='no' frameborder='no' src='https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/#{id}&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true'></iframe>"
      return html.html_safe
    elsif host=='vimeo'
      html = "<div style='padding:56.25% 0 0 0;position:relative;'><iframe src='https://player.vimeo.com/video/#{id}' style='position:absolute;top:0;left:0;width:100%;height:100%;' frameborder='0' allow='autoplay; fullscreen' allowfullscreen></iframe></div><script src='https://player.vimeo.com/api/player.js'></script>"
      return html.html_safe
    # elsif host=='instagram'
    #   html = "<blockquote class='instagram-media' data-instgrm-version='7'><a href='https://www.instagram.com/p/#{id}/'></a></blockquote><script async defer src='//platform.instagram.com/en_US/embeds.js'></script>"
    #   return html.html_safe
    else
      return nil
    end
  end
end
