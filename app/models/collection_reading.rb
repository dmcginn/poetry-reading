class CollectionReading < ActiveRecord::Base
  belongs_to :collection
  belongs_to :reading
end