class Reader < ActiveRecord::Base
  has_many :readings

  extend FriendlyId
  friendly_id :name, use: :slugged

  def image
    # Check file exists - if not, use default
    profile_image_path = Dir["./app/assets/images/profile/#{self.slug}*"].first
    if profile_image_path.nil?
      return ActionController::Base.helpers.asset_path("default_reader.jpg", :digest => false)
    else
      profile_image_file = File.basename(profile_image_path)
      return ActionController::Base.helpers.asset_path("profile/#{profile_image_file}", :digest => false)
    end
  end
end
