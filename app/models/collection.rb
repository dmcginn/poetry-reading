class Collection < ActiveRecord::Base
  has_many :collection_readings, dependent: :delete_all
  has_many :readings, through: :collection_readings

  extend FriendlyId
  friendly_id :name, use: :slugged
end
