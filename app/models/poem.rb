class Poem < ActiveRecord::Base
  belongs_to :poet
  has_many :readings

  extend FriendlyId
  friendly_id :title, use: :slugged
end
