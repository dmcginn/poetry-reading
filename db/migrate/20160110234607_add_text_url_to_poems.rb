class AddTextUrlToPoems < ActiveRecord::Migration[6.0]
  def change
    add_column :poems, :text_url, :string
  end
end
