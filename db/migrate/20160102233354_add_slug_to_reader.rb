class AddSlugToReader < ActiveRecord::Migration[6.0]
  def change
    add_column :readers, :slug, :string
    add_index :readers, :slug, unique: true
  end
end
