class AddMediaTypeToReadings < ActiveRecord::Migration[6.0]
  def change
    add_column :readings, :media_type, :string
  end
end
