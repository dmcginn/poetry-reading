class AddCoverImageToCollections < ActiveRecord::Migration[6.0]
  def change
    add_column :collections, :cover_image, :string
  end
end
