class CreateCollectionReadings < ActiveRecord::Migration[6.0]
  def change
    create_table :collection_readings do |t|
      t.integer :collection_id
      t.integer :reading_id
    end
  end
end
