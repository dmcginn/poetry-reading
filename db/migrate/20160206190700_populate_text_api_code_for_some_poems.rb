class PopulateTextApiCodeForSomePoems < ActiveRecord::Migration[6.0]
  def up
    poems = [
      {'Dulce Et Decorum Est' => 'PM0S44H'},
      {'The Sentry' => 'PMTAVVB'},
      {'Strange Meeting' => 'PMXPW2D'},
      {'The Last Laugh' => 'PM3ORWO'},
      {'Futility' => 'PMDG4FA'},
      {'Mental Cases' => 'PM7IICQ'},
      {'Asleep' => 'PM3Y2VA'},
      {'Anthem for Doomed Youth' => 'PMYKUZE'}
    ]

    # poems.each do |poem|
    #   p = Poem.where(:title => poem.keys.first).first
    #   p.text_api_code = poem[p.title]
    #   p.save!
    # end

  end

  def down
    Poem.update_all( {:text_api_code => nil} )
  end
end
