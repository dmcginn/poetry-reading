class CreateReaders < ActiveRecord::Migration[6.0]
  def change
    create_table :readers do |t|
      t.string :name
      t.string :bio
      t.string :image

      t.timestamps null: false
    end
  end
end
