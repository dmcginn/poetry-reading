class CreateCollections < ActiveRecord::Migration[6.0]
  def change
    create_table :collections do |t|
      t.string :name
      t.string :description
      t.datetime :publish_date

      t.timestamps null: false
    end
  end
end
