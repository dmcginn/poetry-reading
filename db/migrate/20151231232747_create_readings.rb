class CreateReadings < ActiveRecord::Migration[6.0]
  def change
    create_table :readings do |t|
      t.integer :poem_id
      t.integer :reader_id

      t.timestamps null: false
    end
  end
end
