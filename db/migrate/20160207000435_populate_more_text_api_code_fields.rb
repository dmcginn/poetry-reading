class PopulateMoreTextApiCodeFields < ActiveRecord::Migration[6.0]
  def up
    Poem.find_each do |poem|
      next
      title = poem.title

      if (title == "Dublin")
        poem.text_api_code = nil
        poem.save!
        next
      end

      url = "#{Rails.configuration.api_base_url}/v1/poems.json?search=#{title}&api-key=#{Rails.configuration.api_key}"
      url = URI.parse(url)
      req = Net::HTTP::Get.new(url.to_s)
      res = Net::HTTP.start(url.host, url.port) {|http|
        http.request(req)
      }

      response = JSON.parse(res.body)
      poems = response["poems"]
      count = poems.size

      if (count==0)
        # Do nothing
      elsif (count==1)
        poem.text_api_code = poems.first["code"]
      else
        poems.each do |p|
          if (poem.poet.name==p["poet"]["name"])
            poem.text_api_code = p["code"]
          end
        end
      end

      poem.save!

      p "#{poem.title}: #{poem.text_api_code}"
      p response
      p "\n---------------"
    end
  end

  def down
  end
end
