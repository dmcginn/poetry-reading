class AddActiveToReadings < ActiveRecord::Migration[6.0]
  def change
    add_column :readings, :active, :boolean, default: true
  end
end
