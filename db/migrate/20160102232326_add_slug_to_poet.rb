class AddSlugToPoet < ActiveRecord::Migration[6.0]
  def change
    add_column :poets, :slug, :string
    add_index :poets, :slug, unique: true
  end
end
