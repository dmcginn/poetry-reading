class AddTextApiCodeToPoems < ActiveRecord::Migration[6.0]
  def change
    add_column :poems, :text_api_code, :string
  end
end
