require 'csv'

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# TODO: Skip first row

# Seed poets
# Poet.delete_all
CSV.foreach("#{Rails.root}/db/data/poets.csv") do |row|
  next if $.==1
  if !Poet.exists?(:name => row[0])
    Poet.create!(:name => row[0], :image => row[2])
  end
end

# Seed readers
# Reader.delete_all
CSV.foreach("#{Rails.root}/db/data/readers.csv") do |row|
  next if $.==1
  if !Reader.exists?(:name => row[0])
    Reader.create!(:name => row[0], :image => row[2])
  end
end

# Seed poems
# Poem.delete_all
CSV.foreach("#{Rails.root}/db/data/poems_and_readings.csv") do |row|
  next if $.==1
  if !Poem.exists?(:title => row[0])
    poet = Poet.find_by_name(row[1])
    poem = Poem.new(:title => row[0], :poet_id => poet.id)
    poem.save!
  end
end

# Seed readings
# Reading.delete_all
CSV.foreach("#{Rails.root}/db/data/poems_and_readings.csv") do |row|
  next if $.==1

  poem = Poem.find_by_title(row[0])
  poet = Poet.find_by_name(row[1])
  reader = Reader.find_by_name(row[2])
  if !Reading.exists?(:poem_id => poem.id, :reader_id => reader.id, :url => row[3], :media_type => row[4])
    reading = Reading.new(:poem_id => poem.id, :reader_id => reader.id, :url => row[3], :media_type => row[4])
    reading.save!
  end
end