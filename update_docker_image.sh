#!/usr/bin/env bash

cd /home/dave/poetic-voices/
echo 'Pulling latest image';
docker-compose -f docker-compose.yml pull;
echo 'Recreating container';
docker-compose -f docker-compose.yml up -d --force-recreate;
