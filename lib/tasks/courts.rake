require 'net/http'
require 'twitter'
require 'nokogiri'
require 'open-uri'

namespace :courts do
  desc "Tweet court judgements uploaded today"
  task tweet_judgements: :environment do

    # Create Twitter client
    @client = Twitter::REST::Client.new do |config|
      config.consumer_key        = "OV7qbDg226LrvrEeE2ctIXReK"
      config.consumer_secret     = "kPsu4tZvTSqKdtlgzv3QWW88v5Ta8wfyemiyczYreEa0afynYY"
      config.access_token        = "246377283-J6kaH1KAgH1QbCDuhQmfUt4bojJIF2nctWfG1xlM"
      config.access_token_secret = "O1K13LKqfrlsx8fzjFUynE9NWxSpP4dPgw7bvXcS2e7ra"
    end

    # Get latest judgement
    # date = Time.now.strftime("%d/%m/%Y")
    date = Time.now.strftime("%m/%d/%Y")

    url = 'http://www.courts.ie/Judgments.nsf/Webpages/HomePage?OpenDocument'
    url = URI.parse(url)

    source = Net::HTTP.get(url.host, url.path)
    doc = Nokogiri::HTML(source, nil, 'utf-8')

    judgements_array = []

    judgements = doc.css('div.tableformat2 > font > table > tr')
    judgements.each_with_index do |row, index|
      next if index==0

      cells = row.xpath('./td')

      # Prepare tweet
      judgement = {}
      judgement[:url]         = "http://www.courts.ie" + cells[0].xpath('./font/a').attribute('href').value
      judgement[:date]        = cells[0].text
      judgement[:text]        = cells[1].text
      judgement[:court]       = cells[2].text
      judgement[:update_date] = cells[3].text

      judgements_array << judgement
    end

    # Send tweet
    judgements_array.keep_if { |j| j[:update_date]==date }
    judgement = judgements_array.sample

    if judgement
      tweet_text = "#{judgement[:date]} | #{judgement[:court]} | #{judgement[:text]}\n #{judgement[:url]}"
      tweet = @client.update(tweet_text)
      # p tweet
      print "Tweeted judgement #{tweet}\n"
    else
      print "No judgements published today\n"
    end
  end

end
