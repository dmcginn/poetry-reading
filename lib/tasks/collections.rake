require 'csv'
require 'fileutils'

namespace :collections do
  desc "Populate from google"
  task :populate_from_google, [:filename] => :environment do |task, args|

    collections = collections_from_google
    sync_database(collections)

    update_collections
  end
end

def update_collections

  url = 'https://docs.google.com/spreadsheets/d/1MhtDtHUnwGMFXMuSHAWW6qdSYEADFyfpA-1kHIApSeA/export?format=csv&id=1MhtDtHUnwGMFXMuSHAWW6qdSYEADFyfpA-1kHIApSeA&gid=0'
  u = URI.parse(url)

  open(u) do |f|
    f.each_line.with_index do |l, index|
      next if index==0 # Skip header
      CSV.parse(l) do |row|
        reading = Reading.find_by_url(row[3])

        CollectionReading.where(reading_id: reading.id).delete_all

        next if row[7].blank? # Skip if no collections
        collections_in_google = Collection.where(name: row[7].split(",").map(&:strip))
        # collections_to_delete = reading.collections - collections_in_google
        # collections_to_add = collections_in_google - reading.collections

        collections_in_google.each do |c|
          CollectionReading.create(reading_id: reading.id, collection_id: c.id)
        end

        # CollectionReading.where(reading_id: reading.id, collection_id: collections_to_delete.map(&:id)).delete_all
        # collections_to_add.each do |c|
        #   CollectionReading.create(reading_id: reading.id, collection_id: c.id)
        # end
      end
    end
  end

end

def collections_from_google
  collections = []
  url = 'https://docs.google.com/spreadsheets/d/1MhtDtHUnwGMFXMuSHAWW6qdSYEADFyfpA-1kHIApSeA/export?format=csv&id=1MhtDtHUnwGMFXMuSHAWW6qdSYEADFyfpA-1kHIApSeA&gid=995756050'
  u = URI.parse(url)

  open(u) do |f|
    f.each_line.with_index do |l, index|
      next if index==0 # Skip header
      CSV.parse(l) do |row|
        collections << { name: row[0], description: row[1], publish_date: row[2], cover_image: row[3] }
      end
    end
  end

  collections
end

def sync_database(collections)
  Collection.where.not(name: collections.map{|c| c[:name]}).delete_all

  collections.each do |c|
    collection = Collection.find_or_create_by(name: c[:name])
    collection.description = c[:description]
    collection.publish_date = c[:publish_date]
    collection.cover_image = c[:cover_image]
    collection.save!
  end
end











