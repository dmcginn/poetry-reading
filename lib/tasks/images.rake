require 'open-uri'

namespace :images do
  desc "Download profile images from remote URLs to assets folder"
  task download_profiles: :environment do

    local_dir = "#{Dir.pwd}/app/assets/images/profile/"

    # Add all objects to array
    objects = []
    Poet.find_in_batches(batch_size: 100) do |group|
      group.each { |poet| objects << poet }
    end

    Reader.find_in_batches(batch_size: 100) do |group|
      group.each { |reader| objects << reader }
    end

# TODO: First check if file exists, don't overwrite if it does

    objects.each do |object|
      profile_image_path = Dir["./app/assets/images/profile/#{object.slug}*"].first
      if profile_image_path
        puts "#{object.slug} already has an image at #{profile_image_path}"
        next
      end

      if object.read_attribute(:image)

        begin
          url = object.read_attribute(:image)
          extension = File.extname(url)
          filename = "#{object.slug}#{extension}"

          image_data = open(url).read
          raise Exception if image_data.nil?
          open("#{local_dir}#{filename}", 'wb') do |file|
            file << image_data
            puts "Downloaded #{url} to #{local_dir}#{filename}"
          end
        rescue Exception => e
          puts "ERROR: Image #{url} could not be downloaded"
        end

      end
    end

  end
end