namespace :dead_link_check do
  task check: :environment do
    Reading.active.each do |reading|
      puts "Checking #{reading.url}"
      reading.update_attributes(active: false) if reading.dead?
    end
  end
end
