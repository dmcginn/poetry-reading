load "./lib/tasks/data.rake"
load "./lib/tasks/images.rake"
# p Dir.pwd

namespace :update do
  desc "TODO"
  task :all, [:filename] => :environment do |task, args|

    # rake data:add_from_csv[poems_13022016.csv]
    filename = args.filename

    Rake::Task["data:add_from_csv[#{filename}]"].invoke
    Rake::Task["images:download_profiles"].invoke
    Rake::Task["assets:precompile RAILS_ENV=production"].invoke

  end

end
