require 'csv'
require 'fileutils'

namespace :data do
  desc "Add new poems from CSV file"
  # rake data:add_from_csv[poems_13022016.csv]
  task :add_from_csv, [:filename] => :environment do |task, args|
    # Check for filename on command line
    filename = args.filename
    raise "Filename required" unless filename

    file_path = Dir["#{Rails.root}/db/data/#{filename}"].first
    raise "File #{filename} not found" unless file_path

    # Parse CSV
    CSV.foreach(file_path) do |row|
      next if $.==1 # Skip header

      # Poem,Poet,Reader,Link,Media type (video/audio),Poet Profile Image,Reader Profile Image

      poet = Poet.find_or_create_by(name: row[1])
      if row[5]
        poet.image = row[5]
        poet.save!
      end

      reader = Reader.find_or_create_by(name: row[2])
      if row[6]
        reader.image = row[6]
        reader.save!
      end

      poem = Poem.find_or_create_by(title: row[0])
      poem.poet = poet
      poem.save!

      # Insert reading
      unless Reading.exists?(:url => row[3])
        reading = Reading.new(:poem_id => poem.id, :reader_id => reader.id, :url => row[3], :media_type => row[4])
        reading.save!
      end

      # Search / update text_api_code

    end

  end
end


namespace :data do
  desc "Add new poems from Google Docs"
  # rake data:add_from_google
  task :add_from_google => :environment do |task, args|
    url = 'https://docs.google.com/spreadsheets/d/1MhtDtHUnwGMFXMuSHAWW6qdSYEADFyfpA-1kHIApSeA/export?format=csv&id=1MhtDtHUnwGMFXMuSHAWW6qdSYEADFyfpA-1kHIApSeA&gid=0'
    u = URI.parse(url)

    open(u) do |f|
      f.each_line.with_index do |l, index|
        next if index==0 # Skip header
        CSV.parse(l) do |row|
          row = row.map { |x| x&.force_encoding('UTF-8') }
          poet = Poet.find_or_create_by(name: row[1])
          if row[5]
            poet.image = row[5]
            poet.save!
          end

          reader = Reader.find_or_create_by(name: row[2])
          if row[6]
            reader.image = row[6]
            reader.save!
          end

          poem = Poem.find_or_create_by(title: row[0])
          poem.poet = poet
          poem.save!

          # Insert reading
          unless Reading.exists?(:url => row[3])
            reading = Reading.new(:poem_id => poem.id, :reader_id => reader.id, :url => row[3], :media_type => row[4])
            reading.save!
          end
        end
      end
    end

  end
end
