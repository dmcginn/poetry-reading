FROM ruby:2.5
RUN apt-get update -yqq \
    && apt-get install -y \
    nodejs postgresql-client ruby-dev zlib1g-dev liblzma-dev pkg-config libz-dev libgmp-dev libgmp3-dev libc-dev
RUN apt-get install -y vim
COPY . /poetic-voices
WORKDIR /poetic-voices
RUN bundle install --without development test
ENV SECRET_KEY_BASE a657ec0777cf769a9d4c55d4de450b9c50c3c62a629137c870f9b2f72c1fb83cd83aa5da87db9f404ed653e03f52abf739404c844006d0168399ece0de0d396d
ENV RAILS_ENV production
RUN RAILS_ENV=production bundle exec rake assets:precompile
# RUN chmod +x ./entrypoint.sh
# ENTRYPOINT ./entrypoint.sh
